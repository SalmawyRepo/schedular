package com.KMSJ.Scadular.com.Remmitance.controller;


import com.KMSJ.Scadular.com.Remmitance.service.IRemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RemController {


    @Autowired
    IRemService remService;


    @RequestMapping(path = "/getH2Data")
    public ResponseEntity<List> getRemData(){

            return ResponseEntity.ok().body(remService.getRemData());


    }

}
