package com.KMSJ.Scadular.com.Remmitance.repo;

 import com.KMSJ.Scadular.com.app.config.entity.Transaction;
 import org.springframework.beans.factory.annotation.Value;
 import org.springframework.data.repository.CrudRepository;
 import org.springframework.http.ResponseEntity;
 import org.springframework.stereotype.Repository;
 import org.springframework.web.client.RestTemplate;

 import java.text.ParseException;
 import java.text.SimpleDateFormat;
 import java.util.ArrayList;
 import java.util.List;

@Repository
public interface TransactionRepo extends CrudRepository<Transaction,Integer> {

}
