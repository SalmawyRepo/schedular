package com.KMSJ.Scadular.com.Remmitance.service
        ;

import com.KMSJ.Scadular.com.Remmitance.repo.TransactionRepo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface IRemService {



    public List getRemData();
    public  List getRemmitanceTransactions(String url );
    public void saveRemitanceTransactions(List data);
    public int fetchAndSaveTransactions(String url);


}
