package com.KMSJ.Scadular.com.Remmitance.service.impl;


import com.KMSJ.Scadular.com.Remmitance.repo.TransactionRepo;
import com.KMSJ.Scadular.com.Remmitance.service.IRemService;
import com.KMSJ.Scadular.com.app.config.entity.Transaction;
import com.KMSJ.Scadular.com.core.enums.ServiceCodeStatusEnum;
import com.KMSJ.Scadular.com.core.enums.ServiceTypeEnum;
import com.KMSJ.Scadular.com.exception.AggServeResponseExep;
import com.KMSJ.Scadular.com.tracking.service.ITrackingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class service implements IRemService {
    @Value("${app.date.fullFormat}")
    String dateFormate;

    @Autowired
    private ITrackingService trackingService;


    @Autowired
    private TransactionRepo repo;

    @Override
    public List getRemData() {
        return (List) repo.findAll();
    }



public int fetchAndSaveTransactions (String url) throws AggServeResponseExep {





    List remData=null;
    try {
        log.info("Start retrieving  Rem data ....... ");
          remData = getRemmitanceTransactions(url);
        log.info("rem data hase been retrieved :)");
    }catch (Exception ex){
        ex.printStackTrace();
        throw new AggServeResponseExep(ServiceCodeStatusEnum.AGG_102[0], ServiceCodeStatusEnum.AGG_102[1], ServiceTypeEnum.REM_SERVICE);
    }
//================================================================================================================

    try {
    log.info("Start saving  Rem data ....... ");
    saveRemitanceTransactions(remData);
    log.info("rem data hase been saved to database :)");
    }catch (Exception ex){
        ex.printStackTrace();
        throw new AggServeResponseExep(ServiceCodeStatusEnum.AGG_103[0], ServiceCodeStatusEnum.AGG_103[1], ServiceTypeEnum.REM_SERVICE);
    }


   return remData.size();

}

    public  List getRemmitanceTransactions(String url ){



        SimpleDateFormat sdf=new SimpleDateFormat(dateFormate);
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Object[]> response
                = restTemplate.getForEntity(url , Object[].class);


        Object[] result=response.getBody();
        List<Transaction>Transactions=new ArrayList<>();

        for (Object o:result ) {
            List raw= (List) o;
            Transaction tr=new Transaction();
            tr.setId((Integer) raw.get(0));
            tr.setCurrencId(String.valueOf( raw.get(1)));
            BigDecimal bd = new BigDecimal(raw.get(2).toString().replace(",",""));
            tr.setAmount(bd);
            String CptyShorName=(raw.get(4).toString().split("-")[0]);
            tr.setCPTYShortName(CptyShorName);
            log.info("CptyShorName => "+CptyShorName);

            try {
                tr.setValueDate(sdf.parse(String.valueOf( raw.get(3))));
            } catch (ParseException e) {
                tr.setValueDate(null);

            }

            Transactions.add(tr);
        }
        return Transactions;
    }

    @Override
    public void saveRemitanceTransactions(List data) {

for(int i=0;i<data.size();i++){
    Transaction t=(Transaction) data.get(i);
    this.repo.save(t);


}
      //  this.repo.sa(data);
    }


}
