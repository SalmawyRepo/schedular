package com.KMSJ.Scadular.com.tracking.repo;

import com.KMSJ.Scadular.com.app.config.entity.SchedularTrackingStatus;
import com.KMSJ.Scadular.com.core.enums.ServiceCodeStatusEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface SchedularTrackingRepo extends CrudRepository<SchedularTrackingStatus,Integer> {

    @Query("select max(changeDate)  from SchedularTrackingStatus tracker where status like ?1 and serviceType like ?2 ")
    public Date getLastChangeDate(String StatusCode,String serviceType);


}
