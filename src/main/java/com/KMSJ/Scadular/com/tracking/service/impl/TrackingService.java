package com.KMSJ.Scadular.com.tracking.service.impl;

import com.KMSJ.Scadular.com.app.config.entity.SchedularTrackingStatus;
import com.KMSJ.Scadular.com.core.enums.ServiceCodeStatusEnum;
import com.KMSJ.Scadular.com.core.enums.ServiceTypeEnum;
import com.KMSJ.Scadular.com.exception.AggServeResponseExep;
import com.KMSJ.Scadular.com.tracking.repo.SchedularTrackingRepo;
import com.KMSJ.Scadular.com.tracking.service.ITrackingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@Service
@Slf4j
public class TrackingService implements ITrackingService {

    @Autowired
    SchedularTrackingRepo repo;


    @Value("${app.serviceCheck.url}")
    String serviceCheckURL;


    public Date getLastChangeDate(String serviceType){
        Date date=null;
    try {
          date = repo.getLastChangeDate(ServiceCodeStatusEnum.AGG_100[0], serviceType);
        log.info("LastChangeDat => " + date +" Service Type => "+serviceType);
    }catch (Exception e ){
        e.printStackTrace();
    }
        return  date;
    }

    @Override
    public void saveTrackingStatus( String statusCode, String msg,String serviceType,String description )
    {

        SchedularTrackingStatus status=new SchedularTrackingStatus();
        status.setServiceType(serviceType);
        status.setDescription(description);
        status.setMessage(msg);
        status.setStatus(statusCode);

        repo.save(status);




    }

    public boolean isAggreationServiceUp(){
         RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> response
                = restTemplate.getForEntity(serviceCheckURL , String.class);

        try {
            if(response.getStatusCode().value() != HttpStatus.OK.value()){
              throw new AggServeResponseExep(ServiceCodeStatusEnum.AGG_101[0],ServiceCodeStatusEnum.AGG_101 [1], ServiceTypeEnum.AGG_SERVICE);
            }
        }
        catch(Exception e){
            throw new AggServeResponseExep(ServiceCodeStatusEnum.AGG_101[0],ServiceCodeStatusEnum.AGG_101 [1], ServiceTypeEnum.AGG_SERVICE);


        }
return true;

    }


}
