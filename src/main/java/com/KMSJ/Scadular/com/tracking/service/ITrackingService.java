package com.KMSJ.Scadular.com.tracking.service
        ;

import com.KMSJ.Scadular.com.app.config.entity.SchedularTrackingStatus;

import java.util.Date;

public interface ITrackingService {

    public Date getLastChangeDate(String serviceType);

    public void saveTrackingStatus( String statusCode, String msg,String serviceType,String description );
    public boolean isAggreationServiceUp();

}
