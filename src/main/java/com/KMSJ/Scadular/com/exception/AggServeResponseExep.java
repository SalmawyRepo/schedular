package com.KMSJ.Scadular.com.exception;


import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
public class AggServeResponseExep extends RuntimeException {

   private String errorCode;
   private String serviceType;
    private String errorMSG;
    public AggServeResponseExep (String errorCode,String errorMSG,String serviceType){
        super();
        this.errorCode=errorCode;
        this.serviceType=serviceType;
        this.errorMSG=errorMSG;


    }



}
