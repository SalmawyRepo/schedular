package com.KMSJ.Scadular.com.command;

import com.KMSJ.Scadular.com.Kondor.service.IKondorService;
import com.KMSJ.Scadular.com.Remmitance.service.IRemService;
import com.KMSJ.Scadular.com.core.enums.ServiceCodeStatusEnum;
import com.KMSJ.Scadular.com.core.enums.ServiceTypeEnum;
import com.KMSJ.Scadular.com.exception.AggServeResponseExep;
import com.KMSJ.Scadular.com.tracking.service.ITrackingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;


//@Component
@Slf4j
public class RunnerCommand  implements CommandLineRunner {

    @Value("${app.Schedular.msg.success}")
    String successMsg;





    @Value("${app.date.fullFormat}")
    String dateFormate;


    @Value("${app.remitanceSource.url}")
    String RemURL;

  @Value("${app.kondorSource.url}")
    String KonURL;

    @Autowired
    IRemService remService;

  @Autowired
    ITrackingService trackingService;

    @Autowired
    IKondorService kondorService;






    @Override
    @Transactional
    public void run(String... args) throws Exception {
        int count = 0;

        try {
            boolean isServiceUp = trackingService.isAggreationServiceUp();

        } catch (AggServeResponseExep exep) {
            trackingService.saveTrackingStatus(
                    exep.getErrorCode(),
                    exep.getErrorMSG(),
                    exep.getServiceType(),
                    "fail");
            return;
        }
//==========================================================================================================================

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormate);


   /*     log.info("Start retrieving  Last Date form tracking satuts  data ....... ");
        Date lastDate = trackingService.getLastChangeDate();

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        Date tempDate = new Date(c.getTimeInMillis());
        lastDate = (lastDate == null) ? tempDate : lastDate;*/


//============================================================================================================================
/*


        try {
            RemURL ="http://localhost:8089/getRemitance";
            log.info("RemURL is =>  ..  " + RemURL);

            count = remService.fetchAndSaveTransactions(RemURL);
            String desc = "Remittance Transactions  count =>" + count;
            trackingService.saveTrackingStatus(
                    ServiceCodeStatusEnum.AGG_100[0],
                    ServiceCodeStatusEnum.AGG_100[1],
                    ServiceTypeEnum.REM_SERVICE,
                    desc);
        } catch (AggServeResponseExep exep) {
            trackingService.saveTrackingStatus(
                    exep.getErrorCode(),
                    exep.getErrorMSG(),
                    exep.getServiceType(),
                    "fail");

        }
*/


//============================================================================================================================


        KonURL = "http://localhost:8089/getKondorAllDeals";
        log.info("KondURL is =>  ..  " + KonURL);

        try {
            count = kondorService.fetchAndSaveKondorDeals(KonURL);
            String desc = "Kondore Deals   count =>" + count;
            trackingService.saveTrackingStatus(
                    ServiceCodeStatusEnum.AGG_100[0],
                    ServiceCodeStatusEnum.AGG_100[1],
                    ServiceTypeEnum.KONDOR_SERVICE,
                    desc);
        } catch (AggServeResponseExep exep) {
            trackingService.saveTrackingStatus(
                    exep.getErrorCode(),
                    exep.getErrorMSG(),
                    exep.getServiceType(),
                    "fail");

        }
    }
}
