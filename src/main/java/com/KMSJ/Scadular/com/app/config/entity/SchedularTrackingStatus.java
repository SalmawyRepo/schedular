package com.KMSJ.Scadular.com.app.config.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SCHEDULAR_TRACKING_STATUS")
@Getter
@Setter


public class SchedularTrackingStatus {

    public SchedularTrackingStatus() {

        this.changeDate=new Date();
    }
    public SchedularTrackingStatus(String status, Date changeDate, String message, String description) {
        this.status = status;
        this.changeDate = changeDate;
        Message = message;
        this.description = description;
        this.changeDate=new Date();
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue
    Integer id;

    @Column(name = "STATUS")
    String  status;

    @Column(name = "TIMESTAMP")
    Date changeDate;

    @Column(name = "MESSAGE")
    String Message;

    @Column(name = "DESCRIPTION")
    String description;

    @Column(name = "SERVICE_TYPE")
    String serviceType;

}
