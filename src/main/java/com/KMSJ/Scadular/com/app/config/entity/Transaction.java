package com.KMSJ.Scadular.com.app.config.entity;


 import lombok.Getter;
import lombok.Setter;

 import javax.persistence.*;
 import java.math.BigDecimal;
 import java.util.Date;


@Entity
@Table(name = "Remmitance_Transactions")
@Getter
@Setter
/*  "dealId": 110549,
          "id": 7542165,
          "transactionType": "Debit",
          "amount": 50000,
          "currencyCode": "CHF",
          "valueDate": 1581631200000,
          "accountShortName": "234099328"*/
public class Transaction {

    @Id
    @Column(name = "ID")
    @GeneratedValue
    Integer id;

    @Column(name = "CURRENCY")
    String currencId;

    @Column(name = "AMOUNT")
    BigDecimal amount;

    @Column(name = "VALUE_DATE")
    Date ValueDate;

    @Column(name = "CPTY_Short_Name")
    String CPTYShortName;

    @Column(name = "STATUS")
    private    String   status ;


    @Override
    public String toString(){
        return "  id => "+this.id+"  , this. amount =>"+this.getAmount();
    }

}
