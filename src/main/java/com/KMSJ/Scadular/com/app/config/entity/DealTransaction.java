package com.KMSJ.Scadular.com.app.config.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Deals")
@Getter
@Setter
public class DealTransaction {

/*
dealId: 250349,
boDealId: 120208,
id: 289064,
transactionType: "Debit",
amount: 99750,
currencyCode: "GBP",
valueDate: 1609884000000,
accountShortName: "1043975",
description: "Payment for Cashflow not BNotes SpotDeals",
typeOfDeal: "SpotDeals",
rate: 0
        "accountShortName": "234099328"*/


   /* @TableGenerator(name = "TABLE_GENERATOR",table = "ID_TABLE",
            pkColumnName = "ID_TABLE_NAME",
            pkColumnValue = "DEAL_ID",
            valueColumnName = "ID_TABLE_VALUE",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE,generator = "TABLE_GENERATOR")*/

    @Column(name = "ID")
    @Id
    private int paymentId ;
    //--------------------------------------


//--------------------------------------
    @Column(name = "Amount")
    private    double    amount;
//--------------------------------------
    @Column(name = "accountShortName")
    private   String   accountShortName;
//--------------------------------------
    @Column(name = "CURRENCY_CODE")
    private   String   currencyCode;
//--------------------------------------
    @Column(name = "DEAL_ID")
    private  int dealId;
//--------------------------------------
    @Column(name = "TRANSACTION_TYPE")
    private  String transactionType;

//--------------------------------------
    @Column(name = "ValueDate")
    private     Date valueDate;
//--------------------------------------
    @Column(name = "DESCRIPTION")
    private   String   description;

    //--------------------------------------
    @Column(name = "TYPE_OF_DEAL")
    private   String   typeOfDeal;


    //--------------------------------------
    @Column(name = "BO_DEAL_ID")
    private    int    boDealId;

    //--------------------------------------
    @Column(name = "RATE")
    private    double   rate ;
    //--------------------------------------
    @Column(name = "TIMESTAMP_")
    private    Date   timstamp=new Date() ;

    //--------------------------------------
    @Column(name = "CAPTURE_DATE")
    private Date captureDate;

}
