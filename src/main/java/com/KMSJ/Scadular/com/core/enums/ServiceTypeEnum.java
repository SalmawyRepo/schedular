package com.KMSJ.Scadular.com.core.enums;

public class ServiceTypeEnum {

    public static final String REM_SERVICE ="REMITTANCE";
    public static final String KONDOR_SERVICE ="KONDOR";
    public static final String AGG_SERVICE ="aggregation Service";
}
