package com.KMSJ.Scadular.com.core.enums;

public class ServiceCodeStatusEnum {

    public static final String[] AGG_100={"AGG_100","success"};
    public static final String[] AGG_101={"AGG_101","SERVICE IS DOWN"};
    public static final String[] AGG_102={"AGG_102","CANNOT RETRIEVE DATA"};
    public static final String[] AGG_103={"AGG_103","CANNOT SAVE DATA  "};

}
