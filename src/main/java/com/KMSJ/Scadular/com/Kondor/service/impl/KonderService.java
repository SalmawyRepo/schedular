package com.KMSJ.Scadular.com.Kondor.service.impl;


import com.KMSJ.Scadular.com.Kondor.repo.DealsRepo;
import com.KMSJ.Scadular.com.Kondor.service.IKondorService;
import com.KMSJ.Scadular.com.app.config.entity.DealTransaction;
import com.KMSJ.Scadular.com.core.enums.ServiceCodeStatusEnum;
import com.KMSJ.Scadular.com.core.enums.ServiceTypeEnum;
import com.KMSJ.Scadular.com.exception.AggServeResponseExep;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transaction;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class KonderService implements IKondorService {

    @Autowired
    private DealsRepo repo;

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List getKondorDeals() {
        return (List) repo.findAll();
    }





    public  List getKondorDeals(String url ){



        SimpleDateFormat sdf=new SimpleDateFormat("dd-mm-yyyy");
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<DealTransaction[]> response
                = restTemplate.getForEntity(url , DealTransaction[].class);
        //assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));


        DealTransaction[] result=response.getBody();


        return Arrays.asList(result);
    }

    @Override
    @Transactional
    public void saveKondorDeals(List <DealTransaction>data) {
//     //   entityManager.getTransaction().
       this.repo.saveAll(data);
    }
@Override
    public int fetchAndSaveKondorDeals(String url) throws AggServeResponseExep{
  int count=0;
    List kondorData=null;
 //   url="http://localhost:8089/getKondorDeals?from=14-02-2020 00:00:00&to=15-10-2020 00:00:00";
    try {
       log.info("Start retrieving  Kondor deals  ....... ");
        kondorData = getKondorDeals(url);
       log.info("Kondor deals  hase been retrieved :)");
   }catch (Exception ex){
        ex.printStackTrace();
       throw new  AggServeResponseExep(ServiceCodeStatusEnum.AGG_102[0],ServiceCodeStatusEnum.AGG_102[1], ServiceTypeEnum.KONDOR_SERVICE);
   }
//=========================================================================================================================

    try {
    log.info("Start saving  Kondor deals ....... ");
         saveKondorDeals(kondorData);
    log.info("Kondor deals hase been saved to database :)");
    }catch (Exception ex){
        ex.printStackTrace();
        throw new  AggServeResponseExep(ServiceCodeStatusEnum.AGG_103[0],ServiceCodeStatusEnum.AGG_103[1], ServiceTypeEnum.KONDOR_SERVICE);
    }

    return kondorData.size();


}

}
