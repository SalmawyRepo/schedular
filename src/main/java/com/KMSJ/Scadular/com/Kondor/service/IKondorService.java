package com.KMSJ.Scadular.com.Kondor.service
        ;

import com.KMSJ.Scadular.com.Remmitance.repo.TransactionRepo;
import com.KMSJ.Scadular.com.app.config.entity.DealTransaction;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface IKondorService {



    public List getKondorDeals();
    public  List getKondorDeals(String url );
    public void saveKondorDeals(List<DealTransaction> data);

    int fetchAndSaveKondorDeals(String url);
}
