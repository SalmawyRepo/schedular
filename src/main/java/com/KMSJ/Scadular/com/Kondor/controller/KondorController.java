package com.KMSJ.Scadular.com.Kondor.controller;


import com.KMSJ.Scadular.com.Kondor.service.IKondorService;
import com.KMSJ.Scadular.com.Remmitance.service.IRemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class KondorController {


    @Autowired
    IKondorService kondorService;


    @RequestMapping(path = "H2/getKondorDeals")
    public ResponseEntity<List> getRemData(){

            return ResponseEntity.ok().body(kondorService.getKondorDeals());


    }

}
