package com.KMSJ.Scadular.com.Kondor.repo;

 import com.KMSJ.Scadular.com.app.config.entity.DealTransaction;
 import org.springframework.data.repository.CrudRepository;
 import org.springframework.stereotype.Repository;

@Repository
public interface DealsRepo extends CrudRepository<DealTransaction,Integer> {

}
