package com.KMSJ.Scadular.com.Kondor.command;

import com.KMSJ.Scadular.com.Kondor.service.IKondorService;
import com.KMSJ.Scadular.com.Remmitance.service.IRemService;
import com.KMSJ.Scadular.com.core.enums.ServiceCodeStatusEnum;
import com.KMSJ.Scadular.com.core.enums.ServiceTypeEnum;
import com.KMSJ.Scadular.com.exception.AggServeResponseExep;
import com.KMSJ.Scadular.com.tracking.service.ITrackingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


//@Component
@Slf4j
public class CLR implements CommandLineRunner {




    @Value("${app.Schedular.msg.success}")
    String successMsg;

    @Value("${app.date.fullFormat}")
    String dateFormate;


    @Value("${app.remitanceSource.url}")
    String baseRemURL;

    @Value("${app.kondorSource.url}")
    String baseKonURL;

    @Autowired
    IRemService remService;

    @Autowired
    ITrackingService trackingService;

    @Autowired
    IKondorService kondorService;



    @Transactional
    private void fetchRemKonData(){

        SimpleDateFormat sdf=new SimpleDateFormat(dateFormate);
//==========================================================================================================================

        try{
            boolean isServiceUp= trackingService.isAggreationServiceUp();

        }catch(AggServeResponseExep exep){
            trackingService.saveTrackingStatus(
                    exep.getErrorCode(),
                    exep.getErrorMSG(),
                    exep.getServiceType(),
                    "fail");
            return;
        }



//============================================================================================================================
        int count=0;
        Date lastDate=getLastDate(ServiceTypeEnum.REM_SERVICE);


        try{
            String url=baseRemURL;
            //url +="from="+sdf.format(lastDate);
           // url +="&to="+sdf.format(new Date());
            count= remService.fetchAndSaveTransactions(url);
            String desc ="Remittance Transactions  count =>"+count;
            trackingService.saveTrackingStatus(
                    ServiceCodeStatusEnum.AGG_100[0],
                    ServiceCodeStatusEnum.AGG_100[1],
                    ServiceTypeEnum.REM_SERVICE,
                    desc);
        }catch(AggServeResponseExep exep){
            trackingService.saveTrackingStatus(
                    exep.getErrorCode(),
                    exep.getErrorMSG(),
                    exep.getServiceType(),
                    "fail");

        }


//============================================================================================================================

        lastDate=getLastDate(ServiceTypeEnum.KONDOR_SERVICE);
        log.info(sdf.format(lastDate));


        try{
            String url= baseKonURL;
            url+="from="+sdf.format(lastDate);
            url+="&to="+sdf.format(new Date());
            log.info("KondURL is =>  ..  "+ baseKonURL);

            count= kondorService.fetchAndSaveKondorDeals(url);
            String desc ="Kondore Deals   count =>"+count;
            trackingService.saveTrackingStatus(
                    ServiceCodeStatusEnum.AGG_100[0],
                    ServiceCodeStatusEnum.AGG_100[1],
                    ServiceTypeEnum.KONDOR_SERVICE,
                    desc);
        }catch(AggServeResponseExep exep){
            trackingService.saveTrackingStatus(
                    exep.getErrorCode(),
                    exep.getErrorMSG(),
                    exep.getServiceType(),
                    "fail");

        }



    }



    private Date getLastDate(String serviceType){



        log.info("Start retrieving  Last Date form tracking satuts  data ....... ");
        Date lastDate=trackingService.getLastChangeDate(serviceType);

        Calendar c=Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);

        Date tempDate=new Date(c.getTimeInMillis());
        lastDate=(lastDate==null)?tempDate:lastDate;
        return lastDate;
    }
    @Override
    public void run(String... args) throws Exception {

        fetchRemKonData();


    }


}
