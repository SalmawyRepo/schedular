package com.KMSJ.Scadular;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ScadularApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScadularApplication.class, args);
	}


}


